package main;

import java.util.List;

public class Main {
    public static <T> void printArray(T[] arr) {
        for (T t : arr) {
            System.out.println(t);
        }
    }

    public static void main(String[] args) {
        List<Student> students = Student.randomStudentsList();
        for (Student student : students) {
            System.out.println(student);
        }
        students.add(new Student("aaa", "bbb", 23, new int[]{22, 33}));
        System.out.println(students.get(1));
    }
}



package main;

import java.util.function.Function;

public class templates_example {
    public static <T> void printArray(T[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static <T, V> void printArray(T[] arr, V[] brr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        for (int i = 0; i < brr.length; i++) {
            System.out.println(brr[i]);
        }
    }

    public static <T> void sumArray(T[] arr, T defaultValue, Function<T, T> sumFunc) {
        T sum = defaultValue;
        for (int i = 0; i < arr.length; i++) {
//            sum = sumFunc.(sum, arr[i]);
        }
//        return sum;
    }

    public static void main(String[] args) {
        Integer[] arr = {};
        Double[] brr = {};
        printArray(arr);
        printArray(brr);
    }
}

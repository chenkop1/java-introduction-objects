package main;

import java.util.Scanner;

public class conditions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  // Create a Scanner object
        System.out.print("a=");
        int a = scanner.nextInt();
        System.out.print("b=");
        int b = scanner.nextInt();
        if (a % 2 == 0 || b % 2 == 0)
            System.out.println("hello");
        else if (b % 3 == 0 && (a % 3 == 0 || (a + b) % 2 == 0)) {
            b++;
            System.out.println(b);
        } else {
            a++;
            System.out.println(a);
        }
    }
}

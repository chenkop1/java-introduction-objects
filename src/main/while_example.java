package main;

public class while_example {
    public static void main(String[] args) {
        boolean stam = true;
        int k = 0;
        while (stam) {
            stam = (k++) % 2 == 0;
        }
        System.out.println(stam ? "yes" : "no");
    }
}

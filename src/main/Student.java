package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Student {
    private static int amount = 0;
    private String id, name;
    private int age;
    private int[] grades;

    //default constructor
    public Student() {
        Student.amount++;
        this.id = "id0";
        this.name = "name0";
        this.age = 0;
        this.grades = null;
    }

    //copy constructor
    public Student(Student student) {
        Student.amount++;
        this.id = student.id;
        this.name = student.name;
        this.age = student.age;
        this.grades = student.grades;
    }

    //constructor
    public Student(String id, String name, int age, int[] grades) {
        Student.amount++;
        this.id = id;
        this.name = name;
        this.age = age;
        this.grades = grades;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

//    @Override
//    public String toString() {
//        return String.format("[\nid: %s,\nname: %s,\nage: %s,\ngrades: %s\n]", this.id, this.name, this.age, Arrays.toString(this.grades));
//    }

    public double avg() {
        int sum = 0;
        for (int i = 0; i < this.grades.length; i++) {
            sum += this.grades[i];
        }
        return (double) sum / this.grades.length;
    }

    public boolean isOlderThan(Student student) {
//        if (this.age < student.age)
//            return true;
//        return false;
        return this.age < student.age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }

    public static int getAmount() {
        return amount;
    }

    public static Student randomStudent() {
        Random random = new Random();
        String id = "id-" + random.nextInt(10);
        String name = "name-" + random.nextInt(10);
        int age = random.nextInt(50);
        int gradesAmount = random.nextInt(5);
        int[] grades = new int[gradesAmount];
        for (int i = 0; i < grades.length; i++) {
            grades[i] = random.nextInt(100);
        }
        return new Student(id, name, age, grades);
    }

    public static Student[] randomStudentsArray() {
        Random random = new Random();
        Student[] students = new Student[random.nextInt(10)];
        for (int i = 0; i < students.length; i++) {
            students[i] = Student.randomStudent();
        }
        return students;
    }

    public static List<Student> randomStudentsList() {
        Student[] students = Student.randomStudentsArray();
        List<Student> studentsList = new ArrayList<>();
        for (Student student : students) {
            studentsList.add(student);
        }
        return studentsList;
    }
}
package main;

public class for_example {
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            System.out.println(String.format("number %s is %s", i, i * 2));
        }
    }

}

package classes;

public interface Speakable {
    void speak();
}

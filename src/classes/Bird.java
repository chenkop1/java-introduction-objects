package classes;

public abstract class Bird extends Animal {
    private int wings;

    public Bird(int age, int wings) {
        super(age);
        this.wings = wings;
    }

    public int getWings() {
        return wings;
    }

    public void setWings(int wings) {
        this.wings = wings;
    }

    @Override
    public String toString() {
        return "Bird{" +
                "wings=" + wings +
                '}';
    }
}

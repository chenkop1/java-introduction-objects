package classes;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Yona yona = new Yona(123, 22, 55);
        GoldFish goldFish = new GoldFish(12, 33, 55);
        Flamingo flamingo = new Flamingo(12, 33, 22);
        Animal[] animals = {yona, goldFish, flamingo};
        System.out.println(Arrays.toString(animals));
        for (Animal animal : animals) {
            animal.speak();
        }
        Human human = new Human(11, "22", "sss");
        Speakable[] speakables = {flamingo, human, yona};
        for (Speakable speakable : speakables) {
            speakable.speak();
        }
    }
}

package classes;

public abstract class Fish extends  Animal{
    private  int kaskas;

    public Fish(int age, int kaskas) {
        super(age);
        this.kaskas = kaskas;
    }

    public int getKaskas() {
        return kaskas;
    }

    public void setKaskas(int kaskas) {
        this.kaskas = kaskas;
    }

    @Override
    public String toString() {
        return "Fish{" +
                "kaskas=" + kaskas +
                '}';
    }
}

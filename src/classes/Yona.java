package classes;

public class Yona extends Bird {
    private int strength;

    public Yona(int age, int wings, int strength) {
        super(age, wings);
        this.strength = strength;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public String toString() {
        return "Yona{" +
                "strength=" + strength +
                '}';
    }

    @Override
    public void speak() {
        System.out.println("bituach yashir");
    }
}

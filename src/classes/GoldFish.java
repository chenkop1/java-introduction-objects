package classes;

public class GoldFish extends Fish {
    private int goldish;

    public GoldFish(int age, int kaskas, int goldish) {
        super(age, kaskas);
        this.goldish = goldish;
    }

    public int getGoldish() {
        return goldish;
    }

    public void setGoldish(int goldish) {
        this.goldish = goldish;
    }

    @Override
    public String toString() {
        return "GoldFish{" +
                "goldish=" + goldish +
                '}';
    }

    @Override
    public void speak() {
        System.out.println("blublu");
    }
}

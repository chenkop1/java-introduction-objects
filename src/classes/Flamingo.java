package classes;

public class Flamingo extends  Bird {
    private int pink;

    public Flamingo(int age, int wings, int pink) {
        super(age, wings);
        this.pink = pink;
    }

    public int getPink() {
        return pink;
    }

    public void setPink(int pink) {
        this.pink = pink;
    }

    @Override
    public String toString() {
        return "Flamingo{" +
                "pink=" + pink +
                '}';
    }

    @Override
    public void speak() {
        System.out.println("pink pink");
    }
}
